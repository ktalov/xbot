<?php
header('Access-Control-Allow-Origin: *');
header('Accept: */*');
error_reporting(E_ALL);
ini_set('display_errors', 1);
//session_start();

require_once("Bitmex.php");

class Surf extends BitMex
{
	function __construct($key='', $secret='', $timeframe='1m', $leverage = 1, $margin=1) 
	{
		$this->bitmex = new BitMex($key, $secret);
		$this->bitmex->setLeverage($leverage);
		$this->TIMEFRAME = $timeframe;
		$this->MARGIN = $margin;
		$this->LEVERAGE = $leverage;

		/*localhost*/
		$this->SQL_SERVER = 'localhost';
		$this->SQL_USER = 'root';
		$this->SQL_PASS = '';
		$this->DATABASE_NAME = 'surf';
	}

	public function writeHistory($what='-')
	{
		/*Escribe en el historico del proyecto
		* @param what -> Lo que quieres escribir
		*/
		$logTime = "surfRegister.html";
		$openLog = fopen($logTime, "a+");
		$inputData = $what."<br>";
		fwrite($openLog, $inputData);
		fclose($openLog);
	}

	public function test($what='-')
	{
		/*Escribe en el historico del proyecto
		* @param what -> Lo que quieres escribir
		*/
		$logTime = "surfTest.html";
		$openLog = fopen($logTime, "a+");
		$inputData = $what."<br>";
		fwrite($openLog, $inputData);
		fclose($openLog);
	}

	private function saveTrade($date, $open, $high, $low, $close, $type, $roe)
	{
		$conn = new mysqli($this->SQL_SERVER, $this->SQL_USER, $this->SQL_PASS, $this->DATABASE_NAME);
		mysqli_autocommit($conn, TRUE);

		$sql = "INSERT INTO trades (date, open, high, low, close, type, margin, leverage, roe) VALUES ('$date',$open,$high,$low,$close,'$type',$this->MARGIN,$this->LEVERAGE,$roe);";
		mysqli_query($conn, $sql);

		mysqli_close($conn);
	}

	private function getWalletAmount()
	{
		return $this->bitmex->getWallet()['amount']/100000000;
	}

	public function getNCandles($numCandles)
	{
		while (true){
			$candles = $this->bitmex->getCandles($this->TIMEFRAME, $numCandles);
			if (count($candles)<$numCandles){
				sleep(2);
				continue;
			}else{
				break;
			}
		}
		return $candles;
	}

	public function getMarketPrice()
	{
		return $this->bitmex->getTicker()['last'];
	}

	private function calculateRoe()
	{
		/*Calcula el ROE de las posiciones abiertas*/

		$positions = $this->bitmex->getOpenPositions();
		$roe = 0.0;
		if (count($positions)>0){
			$vals = array();
			/*Calcula el ROE general de las posiciones abiertas*/
			for ($i=0;$i<count($positions);$i++){
				array_push($vals, $positions[$i]['unrealisedRoePcnt']);
			}
			if (count($vals)>0){
				$roe = array_sum($vals)/count($vals);
				$roe = $roe * 100;
			}
		}
		return $roe;
	}

	public function closeAnyPosition()
	{
		/*Calcular ROE*/
		$ROE = $this->calculateRoe();

		/*Efectar venta de cualquier contrato*/
		//$this->bitmex->closePosition(null);
		$this->writeHistory("Sell | <b class='good'> - ROE = $ROE</b> ");
	}

	public function makeOrder($orderType, $fi, $high, $low, $close, $open)
	{
		/*Calcular ROE*/
		$ROE = $this->calculateRoe();

		/*Creaer nueva orden*/
		if ($orderType == 'LONG'){
			//$this->bitmex->createOrder("Market", "Buy", null, $this->MARGIN);
			$this->writeHistory("<b>Long | Order at ".date("d-m-Y H:i:sa")." in candle $fi</b><br>");
			$this->test("<b>Long | Order at ".date("d-m-Y H:i:sa")." in candle $fi</b><br>");
		}else if($orderType == 'SHORT'){
			//$this->bitmex->createOrder("Market", "Sell", null, $this->MARGIN);
			$this->writeHistory("<b>Short | Order at ".date("d-m-Y H:i:sa")." in candle $fi</b><br>");
			$this->test("<b>Short | Order at ".date("d-m-Y H:i:sa")." in candle $fi</b><br>");
		}else{
			$this->writeHistory("X | <b class='bad'>X</b> at ".date("d-m-Y H:i:sa")." in candle $fi");
		}

		$this->saveTrade($fi, $open, $high, $low, $close, $orderType, $ROE);
		$tab = "<table border=1>
					<caption>Nueva Orden</caption>
					<tbody>
						<tr>
							<td>FI</td>
							<td>OPEN</td>
							<td>HIGH</td>
							<td>LOW</td>
							<td>CLOSE</td>
							<td>TYPE</td>
							<td>ROE</td>
						</tr>
						<tr>
							<td>$fi</td>
							<td>$open</td>
							<td>$high</td>
							<td>$low</td>
							<td>$close</td>
							<td>$orderType</td>
							<td>$ROE</td>
						</tr>
					</tbody>
				</table>";
		$this->writeHistory($tab);
		$this->test($tab);
	}

	public function getMatrixOrder($progressOrder)
	{
		$LONGS = array();
		$SHORTS = array();
	}

	public function getCandleColor($progressOrder, $open, $close)
	{
		return array('color'=>'','size'=>'');
	}

	public function runSurf($fi, $phase, $open, $close, $high, $low, $orderType='N/A')
	{
		if ($phase == 1){
			/*Esta fase toma 2 valores y determina parcialmente cual es la pendiente que sigue la curva*/
			/*Solicita 2 candles*/
			$candles = $this->getNCandles(2);
			$closeA = ($candles[0]['close']+$candles[0]['open']+$candles[0]['high']+$candles[0]['low'])/4;
			$closeB = $candles[1]['close'];

			/*Preparando los nuevos valores para cuando vuelva a ser llamada la clase.Se toma la fecha anterior a la nueva o que se esta calculando.*/
			/*Determina que hacer de acuerdo al valor de los candles obtenidos*/
			if ($closeA >= $closeB){
				$orderType = 'LONG';
			}else{
				$orderType = 'SHORT';
			}
			$fi = $candles[1]['timestamp'];
			$open = $candles[1]['open'];
			$close = $candles[1]['close'];
			$high = $candles[1]['high'];
			$low = $candles[1]['low'];

			/*Crear una orden*/
			$this->makeOrder($orderType, $fi, $high, $low, $close, $open);

			return array('ret_message'=>"Fase 1 ejecutada",'ret_status'=>true,'ret_candles'=>$candles,'fi'=>$fi,'phase'=>2,'high'=>$high,'low'=>$low,'close'=>$close,'open'=>$open,'orderType'=>$orderType,'WALLET_AMOUNT'=>$this->getWalletAmount());
		}else{
			/*Recepcion de data anterior*/
			$last_fi = $fi;
			$last_open = $open;
			$last_close = $close;
			$last_high = $high;
			$last_low = $low;
			$last_phase = $phase;
			$last_orderType = $orderType;

			/*Solicitar 2 candles*/
			$candles = $this->getNCandles(2);

			/*Verifica la fecha de cuando se efectuo la operacion anterior existe.Si existe es porque aun no se ha cerrado el candle que le seguia.El candle que seguia al ser cerrado es lo que tomamos como indicador para saber si hacer SELL o continuar la pendiente a favor*/
			$EXIST = false;
			for ($h = 0; $h<count($candles); $h++){
				if ($last_fi === $candles[$h]['timestamp']){
					$EXIST = true;
					break;
				}else{
					continue;
				}
			}

			if ($EXIST){
				$ret_message = "El Trigger-Candle aun no ha cerrado<br>";
				$this->test($ret_message);
				/*Mantener el dato justo como llego*/
				return array('ret_message'=>$ret_message,'ret_status'=>true,'ret_candles'=>$candles,'fi'=>$last_fi,'phase'=>2,'high'=>$last_high,'low'=>$last_low,'close'=>$last_close,'open'=>$last_open,'orderType'=>$last_orderType,'WALLET_AMOUNT'=>$this->getWalletAmount());
			}else{
				/*SI YA NO EXISTE, quiere decir que el candle que le sigue ya CERRO.Ese candle deberia ser ,en el mejor caso,el de fecha mas vieja es decir el $candle[1]*/
				$TRIGGER_CANDLE = $candles[1];
				$trig_close = $TRIGGER_CANDLE['close'];

				/*Verifica cual era el tipo de orden efectuada anteriormente y determina que hacer en base a la tendencia de trade actual*/
				$this->test("<b>$last_close</b> | <b>$trig_close</b> | BEFORE <b>$last_orderType</b>");
				$this->writeHistory("<b>$last_close</b> | <b>$trig_close</b> | BEFORE <b>$last_orderType</b>");

				$roe = strval($this->calculateRoe());
				switch($last_orderType){
					case 'LONG':
						if ( ($last_close > $TRIGGER_CANDLE['close']) and (abs($TRIGGER_CANDLE['close']-$TRIGGER_CANDLE['open'])>0.5) ){
							/*Cambio de trade, paso a SHORT*/
							$orderChange = true;
							$newOrderType = 'SHORT';
							$ret_message = "Cambio de ORDEN > $last_orderType to $newOrderType";
						}else{
							$orderChange = false;
							$newOrderType = $last_orderType;
							$ret_message = "Long continue in progress... > Profit of $roe | $last_orderType";
						}
						break;
					case 'SHORT':
						if ( $last_close < $TRIGGER_CANDLE['close'] and (abs($TRIGGER_CANDLE['close']-$TRIGGER_CANDLE['open'])>0.5) ){
							/*Cambio de trade, paso a LONG*/
							$orderChange = true;
							$newOrderType = 'LONG';
							$ret_message = "Cambio de ORDEN > $last_orderType to $newOrderType";
						}else{
							$orderChange = false;
							$newOrderType = $last_orderType;
							$ret_message = "Short continue in progress... > Profit of $roe | $last_orderType";
						}
						break;
					default:
						$ret_message = "-No hay orden especificada-Se determinara una orden.";

						/*Sino hay un tipo de orden especificada entonces se determina uno*/
						$this->test($ret_message);
						$this->writeHistory($ret_message);

						/*Esta fase toma 2 valores y determina parcialmente cual es la pendiente que sigue la curva*/
						/*Solicita 2 candles*/
						$candles = $this->getNCandles(2);
						$closeA = ($candles[0]['close']+$candles[0]['open']+$candles[0]['high']+$candles[0]['low'])/4;
						$closeB = $candles[1]['close'];

						/*Preparando los nuevos valores para cuando vuelva a ser llamada la clase.Se toma la fecha anterior a la nueva o que se esta calculando.*/
						/*Determina que hacer de acuerdo al valor de los candles obtenidos*/
						if ($closeA >= $closeB){
							$orderType = 'LONG';
						}else{
							$orderType = 'SHORT';
						}
						$fi = $candles[1]['timestamp'];
						$open = $candles[1]['open'];
						$close = $candles[1]['close'];
						$high = $candles[1]['high'];
						$low = $candles[1]['low'];

						/*Crear una orden*/
						$this->closeAnyPosition();
						$this->makeOrder($orderType, $fi, $high, $low, $close, $open);

						return array('ret_message'=>$ret_message,'ret_status'=>true,'ret_candles'=>$candles,'fi'=>$fi,'phase'=>2,'high'=>$high,'low'=>$low,'close'=>$close,'open'=>$open,'orderType'=>$orderType,'WALLET_AMOUNT'=>$this->getWalletAmount());
				}

				$this->test($ret_message);
				$this->writeHistory($ret_message);

				/*Verifica si hay que cambiar la orden*/
				if ($orderChange == true){
					$this->closeAnyPosition();
					$this->makeOrder($newOrderType, $TRIGGER_CANDLE['timestamp'], $TRIGGER_CANDLE['high'], $TRIGGER_CANDLE['low'], $TRIGGER_CANDLE['close'], $TRIGGER_CANDLE['open']);
				}

				return array('ret_message'=>$ret_message,'ret_status'=>true,'ret_candles'=>$candles,'fi'=>$TRIGGER_CANDLE['timestamp'],'phase'=>$last_phase,'high'=>$TRIGGER_CANDLE['high'],'low'=>$TRIGGER_CANDLE['low'],'close'=>$TRIGGER_CANDLE['close'],'open'=>$TRIGGER_CANDLE['open'],'orderType'=>$newOrderType,'WALLET_AMOUNT'=>$this->getWalletAmount());
			}
		}
	}
}

if ($_SERVER["REQUEST_METHOD"] == 'GET'){
	if (isset($_GET['strategy'])){
		switch($_GET['strategy']){
			case 'SURF_BOT':
				$bot = new Surf($_GET['key'], $_GET['secret'], $_GET['timeframe'], $_GET['leverage'], $_GET['margin'] );
				$res = $bot->runSurf($_GET['fi'], $_GET['phase'], $_GET['open'],$_GET['close'],$_GET['high'],$_GET['low'], $_GET['orderType']);
				break;
			default:
				$res = array('ANSWER'=>'N/A');
		}
	} else {
		$res = array('ANSWER'=>'N/A');
	}
	//Retornar el resultado
	echo json_encode($res);
}
?>