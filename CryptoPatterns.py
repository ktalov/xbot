import math
import os
import matplotlib.pyplot as plt
import numpy as np

class PATTERNS_LOOKER:


    def __init__(self):

        self.xfiles     = []
        self.xdata      = []
        self.xdigits    = []
        self.patterns   = []
        self.min_percent = 0.0

    def set_xfiles(self, files, method):
        """
        Establece cuales son los archivos a trabajar. files -> [file1,file2,...] ; method -> joined|aparted
        """
        
        if type(files) == list:
            sfile = open('joined.txt', 'w')
            for f in files:
                if method.lower() == 'joined':
                    self.xfiles = ['joined.txt']
                    #-- Abre el archivo en modo lectura
                    mfile = open(os.getcwd() + '\%s'%f, 'r')
                    #-- Leer linea a linea el archivo
                    while True:
                        val = mfile.readline()
                        if not val:
                            break
                        else:
                            val = val.strip()
                            sfile.write(val + '\n')
                    mfile.close()
                else:
                    self.xfiles.append(f)
            sfile.close()
        else:
            self.xfiles = [None]
            
    def load_data(self):
        """
        Carga la informacion presente en los archivos
        """
        
        if type(self.xfiles) == list:
            if len(self.xfiles) >= 1:
                if self.xfiles[0] != None:
                    for x in self.xfiles:
                        #-- Coloca un 0 al inicio de cada lista de valores
                        self.xdata.append([0])
                        #-- Abre el archivo en modo lectura
                        mfile =  open(os.getcwd() + '\%s'%x, 'r')
                        lx = 0
                        #-- Leer linea a linea el archivo
                        while True:
                            val = mfile.readline()
                            lx += 1
                            if not val:
                                break
                            else:
                                try:
                                    val = float(val.strip().replace(',','.'))
                                except:
                                    print('The point [%s] in file %s is not valid. Near of line %d'%(str(val), str(x), lx))
                                #-- Formato con 1 sola cifra decimal
                                val = '%.1f'%val
                                self.xdata[len(self.xdata) - 1].append(float(val))
                        #-- Cierra el archivo
                        mfile.close()
                else:
                    print ('You must to indicate al least some data file')
            else:
                print ('You have not indicate any file')
        else:
            print ('The data passed must by a list of files and not %s'%type(self.xfiles))
    
    def convert_to_digital(self):
        """
        Convierte los valores flotantes en 1,0 o -1 de acuerdo a la secuencia de Mayor,Menor,Constancia
        de los datos
        """
        
        for x in self.xdata:
            self.xdigits.append([0])
            for y in range(1, len(x), 1):
                if x[y] > x[y-1]:
                    self.xdigits[len(self.xdigits)-1].append(1)
                elif x[y] < x[y-1]:
                    self.xdigits[len(self.xdigits)-1].append(-1)
                else:
                    self.xdigits[len(self.xdigits)-1].append(0)

    def config_sizes(self, min_block, max_block, len_list):
        """
        Valida y/o configura rango de bloques.
        En modo auto se toma min_block = 1  max_block = 2*min_block + 1
        """

        #-- Certifica valor del bloque menor
        if min_block == 'auto':
            min_block = int(math.ceil(len_list*0.25*0.25))
        try:
            min_block = int(math.ceil(float(min_block)))
            if min_block <= 0:
                print ('MIN_BLOCK debe ser mayor que 0(cero)')
                raise ValueError
        except:
            print ('El valor de MIN_BLOCK no es correcto, se asigna 1')
            min_block = 1
            
        #-- Certifica valor del bloque mayor
        if max_block == 'auto':
            #max_block = int(len(self.xdigits)/2)
            max_block = 2*min_block + 1
        else:
            try:
                max_block = int(math.ceil(float(max_block)))
            except:
                print ('El valor de MAX_BLOCK no es correcto')
                raise ValueError
        if max_block < min_block:
            print ('Max_Block = %s debe ser mayor que Min_Block  = %s'%(str(max_block), str(min_block)))
            raise ValueError

        #-- Intenta evitar que min_block sea 3.Se hace esto porque con bloques de 3 solo tendrias 2 datos para predecir la siguiente jugada lo cual praticamente
        #no es suficiente para tomar una decision.O lo minimo requeririamos bloques de 3.
        if min_block <= 3:
            if max_block - min_block >= 2:
                min_block += 1
                
        print ('MIN_BLOCK = %f\nMAX_BLOCK = %f\n'%(min_block, max_block))

        return min_block, max_block
    
    def get_blocks(self, min_block, max_block):
        """
            Crea bloques de datos del registro suministrado
            min_block : Hace referencia al menor tamano de bloque que se desea comparar (0 < int < max_block)
            max_block : Hace referencia al mayor tamano de bloque que desea comparar (int|auto > min_block)
            #-- Mientras menos es MIN_BLOCK mas ordenes de compra y/o venta se generaran, esto no es lo mejor en todo caso.
            NOTA : Utilizar bloques muy pequenos no es la mejor opcion, mientras mayor es el conjunto de datos lo ideal es ampliar el tamano minimo de bloque.
                    Como modo automatico, se asigna 1/16 de la totalidad de elementos. Es decir, (1/4) de (1/4) de la totalidad ==> N*(1/4)*(1/4)
        """

        print ('Extrayendo bloques...')
        self.xdigits = self.xdigits[0]
        self.xdata = self.xdata[0]
        BLOCKS_DIGITALES = []
        BLOCKS_DATA = []

        #-- Configurar tamano de bloques
        max_size = len(self.xdigits)
        min_block, max_block = self.config_sizes(min_block, max_block, max_size)
    
        #-- Extraccion de bloques
        while True:
            if min_block <= max_block:
                dig_blocks = []
                dat_blocks = []
                ind = 0
                while True:
                    if ind < max_size:
                        #-- Anade un nuevo bloque de la secuencia
                        dig_blocks.append(self.xdigits[ind:ind + min_block])
                        dat_blocks.append(self.xdata[ind:ind + min_block])
                        #-- En el proximo paso a stp se le suma la longitud del bloque menor haciendo que en cada ciclo la extraccion inicie en otro bloque
                        #ind += min_block
                        #-- En el proximo paso a stp se le suma 1
                        ind += 1
                    else:
                        break
                    
                #-- Agrega la agrupacion de bloques obtenidas de principio a fin de datos
                BLOCKS_DIGITALES.append(dig_blocks)
                BLOCKS_DATA.append(dat_blocks)

                #-- Incrementa el tamano de bloque, esto es mientras sea menor o igual que el maximo tamano de bloque
                min_block += 1
            else:
                break
            
        del dig_blocks
        del dat_blocks
        return BLOCKS_DIGITALES, BLOCKS_DATA

    def calculate_percent(self, a, b):
        """
        Retorna el porcentaje de comparacion de 2 arrays digitales.Se comparan los de cada lista cada uno con su homologo.
        Sean las series A = 1111 ; B= 0010
        Entre A y B ,el porcentaje de similitud es de un 25% dado que entre ambas, posicionalmente, solo coincide 1 elemento en ambos (el 1), por lo tanto,
        dado que son 4 elementos y solo 1 coincide
        1/4 = 0.25 = 25%
        """
        percent = float('%.4f'%0.0)
        if type(a) == list and type(b) == list:
            if len(a) != len(b):
                percent = float('%.4f'%0.0)
                return percent
        else:
            print ('A y B deben ser arreglos de longitud equivalente')
            raise IOError

        match = 0
        total = len(a)
        #-- Verifica que el ultimo elemento de cada lista sean iguales,es decir,el porcerntaje de comparacion de los ultimos elementos debe ser de un 100%
        #-- Esto garantiza que ambos patrones al final sean ambos subidas ,bajadas o bien constancias.
        if a[len(a) - 1] == b[len(b) - 1]:
            for x in range(0, total, 1):
                if a[x] == b[x]:
                    match += 1
                else:
                    None
        else:
            percent = float('%.4f'%0.0)
            
        #-- Calcula el porcentaje
        try:
            percent = float('%.4f'%(float(match)/float(total)))
        except ZeroDivisionError:
            percent = float('%.4f'%0.0)

        return percent
    
    def get_patterns(self, digits, data, min_percent):
        """
        Obtiene los patrones en las series
        Sean las series A = 1111 ; B= 0010
        Entre A y B ,el porcentaje de similitud es de un 25% dado que entre ambas, posicionalmente, solo coincide 1 elemento en ambos (el 1), por lo tanto, dado que son 4 elementos y solo 1 coincide
        1/4 = 0.25 = 25%
        Para que 2 muestras sean iguales debera existir al menos un min_percent% de parecido.
        Devuelve una lista con los patrones y la cantidad de repeticiones de cada uno.
        
        NOTA :  Lo ideal es utilizar porcentajes cercanos al 100%.
                Mientras mas cerrado a 1 mejor.Un bloque se compara con los demas pero el que se guarda como valido es el base y no los demas.
                En otras palabras, mientras mas cercano a 1 es mas exacto y por tanto son las ordenes mas ideales.
        """
        print ('Comparando bloques...')
        self.min_percent = min_percent
        reps = {}
        ldig = len(digits)
        counter = 0
        #-- Indice que ocupan los bloques de longitud N en la lista digits y/o data
        for blocks in digits:
            len_blocks = len(blocks)
            ind = 0
            counter += 1
            print ('Block %d / %d '%(counter, ldig))
            #-- Toma uno a uno los elementos de Blocks
            for part_block in blocks:
                #-- Usa ind + 1 en el siguiente FOR para comparar el elemento actual con todos los siguientes a partir de su posicion
                #ind + 1
                #-- Usa 0 en el siguiente FOR para comparar el elemento actual con todos los siguientes desde el inicio
                #0
                for next_elem in blocks[0:len_blocks]:
                    #-- Verifica si el porcentaje de comparacion es adecuado
                    if len(part_block) == len(next_elem):
                        if self.calculate_percent(part_block, next_elem) >= min_percent:
                            try:
                                #--Suma 1 al valor de la clave existente
                                reps.update({str(part_block):reps[str(part_block)] + 1})
                            except KeyError:
                                #-- Anade la nueva clave al diccionario
                                #-- Se coloca 2 porque al momento de comparar un primer elemento existe el elemento y su homologo, por lo tanto hemos de iniciar el conteno en 2
                                reps.update({str(part_block):2})
                        else:
                            continue
                    else:
                        continue
                ind += 1
        print ('>Completados<')
        return reps

    def get_total_groups(self, len_pattern, digits):
        """
        Retorna la cantidad de bloques de longitud PATTERN que pueden obtenerse a partir de un grupo de datos
        """
        ld = len(digits)
        total = 0.0
        ind_desp = -1
        while True:
            ptt = []
            ind_desp += 1
            if ind_desp >= ld - 1:
                break
            else:
                for x in digits[ind_desp:]:
                    ptt.append(x)
                    if len(ptt) == len_pattern:
                        total += 1
                        ptt = []
                    else:
                        continue
        return total
    
    def get_sell_list(self, reps, digits, min_percent, print_it):
        """
        Retorna la lista de patrones apropiados para efecutar ventas
        reps: Dictionario de patrones
        min_percent : A partir de que porcentaje de aparicion en todo el conjunto se toman los patrones buenos para ventas
        print_it : Indica si quiere imprimir o no los resultados.
        OJO .... RECUERDA QUE EL VALOR FINAL ES LO QUE SE ESPERA,POR EJEMPLO , LA SECUENCIA -1~1~-1~1~-1 INDICA QUE LUEGO DE -1~1~-1~1~ SE ESPERA UNA CAIDA CON UN X% de probabilidad
        """
        # Porcentaje = (total de repeticiones del bloque [x,x,x..])/(total de bloques [x,x,x..] que pueden crearse)
        # total de bloques [x,x,x..] que pueden crearse = (n-1)+(n-2)+....+r donde n es la cantidad de elementos de digits y R es la cantidad de elementos que desea agrupar

        print ('Extrayendo patrones de subidas...')
        #-- Identifica los patrones para ventas
        sell_per = []
        sell_ser = []
        for key in reps:
            lt = key.replace("'", '').replace('[', '').replace(']', '').replace(' ', '').split(',')
            #-- total de bloques [x,x,x..] que pueden crearse = (n-1)+(n-2)+....+ r donde n es la cantidad de elementos de digits y R es la cantidad de elementos que desea agrupar
            #print reps[key]
            try:
                percent = float('%.4f'%(float(reps[key])/self.get_total_groups(len(lt), self.xdigits)))
            except ZeroDivisionError:
                percent = float('%.4f'%0.0)

            #-- Condiciones que terminan en subidas
            if lt[len(lt) - 1] == '1':
                if percent >= min_percent:
                    sell_ser.append(key)
                    sell_per.append(percent)

            #--Condiciones que van en subida y terminan constantes
            if lt[len(lt) - 1] == '0':
                if '1' in lt or '-1' in lt:
                    rev = lt
                    rev.reverse()
                    ind = -1
                    #-- Verifica que antes de cualquier secuencia de 0's exista inmediatamente un 1.Si es asi y el porcentaje es valido, lo agrega.
                    while True:
                        ind += 1
                        if rev[ind] == '1':
                            if percent >= min_percent:
                                sell_ser.append(key)
                                sell_per.append(percent)
                            break
                        elif rev[ind] == '-1':
                            break
                        else:
                            continue
                else:
                    None
                    
        #-- Ordena la mezclas de las listas de menor a mayor basado en los patrones con mayores porcentajes
        zipp = zip(sell_ser, sell_per)
        sell_list = sorted(zipp, key = lambda x: x[1])
        
        #-- Invierte la lista de mayor a menor
        sell_list.reverse()
        out = open('0a_to_up_patterns.txt', 'w')
        #out.write('Patron,Probabilidad,Comentario\n')
        for q in sell_list:
            ex = q[0].replace('[', '').replace(']', '').replace(' ', '') + ',' + str(q[1]*100) + '%'
            if print_it == True:
                print (q[0], str(q[1]*100) + '%')
            out.write(ex + '\n')
        out.close()
        print ('\tPatrones de subidas completadas >>> %d'%len(sell_list))
        return sell_list

    def get_buy_list(self, reps, digits, min_percent, print_it):
        """
        Retorna la lista de patrones apropiados para efecutar compras
        reps: Dictionario de patrones
        min_percent : A partir de que porcentaje de aparicion en todo el conjunto se toman los patrones buenos para compras
        print_it : Indica si quiere imprimir o no los resultados
        OJO .... RECUERDA QUE EL VALOR FINAL ES LO QUE SE ESPERA,POR EJEMPLO , LA SECUENCIA -1~1~-1~1~-1 INDICA QUE LUEGO DE -1~1~-1~1~ SE ESPERA UNA CAIDA CON UN X% de probabilidad
        """

        # Porcentaje = (total de repeticiones del bloque [x,x,x..])/(total de bloques [x,x,x..] que pueden crearse)
        # total de bloques [x,x,x..] que pueden crearse = (n-1)+(n-2)+....+r donde n es la cantidad de elementos de digits y R es la cantidad de elementos que desea agrupar

        print ('Extrayendo patrones de caidas...')
        #-- Identifica los patrones para compras
        #-- Porcentajes
        buy_per = []
        #-- Series
        buy_ser = []
        for key in reps:
            lt = key.replace("'", '').replace('[', '').replace(']', '').replace(' ', '').split(',')
            
            #-- total de bloques [x,x,x..] que pueden crearse = (n-1)+(n-2)+....+r donde n es la cantidad de elementos de digits y R es la cantidad de elementos que desea agrupar
            try:
               percent = float('%.4f'%(float(reps[key])/self.get_total_groups(len(lt), self.xdigits)))
            except ZeroDivisionError:
                percent = float('%.4f'%0.0)
                
            #-- Condiciones que acaban en bajadas
            if lt[len(lt) - 1] == '-1':
                if percent >= min_percent:
                    buy_ser.append(key)
                    buy_per.append(percent)
                    
            #--Condiciones que van en bajada y terminan constantes
            if lt[len(lt) - 1] == '0':
                if '1' in lt or '-1' in lt:
                    rev = lt
                    rev.reverse()
                    ind = -1
                    #-- Verifica que antes de cualquier secuencia de 0's exista inmediatamente un -1.Si es asi y el porcentaje es valido, lo agrega.
                    while True:
                        ind += 1
                        if rev[ind] == '-1':
                            if percent >= min_percent:
                                buy_ser.append(key)
                                buy_per.append(percent)
                            break
                        elif rev[ind] == '1':
                            break
                        else:
                            continue
                        
        #-- Ordena la mezclas de las listas de mayor a menor basado en los patrones con mayores porcentajes
        zipp = zip(buy_ser, buy_per)
        buy_list = sorted(zipp, key = lambda x: x[1])
        
        #-- Invierte la lista de mayor a menor
        buy_list.reverse()
        out = open('0a_to_down_patterns.txt', 'w')
        #out.write('Patron,Probabilidad,Comentario\n')
        for q in buy_list:
            ex = q[0].replace('[', '').replace(']', '').replace(' ', '') + ',' + str(q[1]*100) + '%'
            if print_it == True:
                print (q[0], str(q[1]*100) + '%')
            out.write(ex + '\n')
        out.close()
        print ('\tPatrones de caidas completadas >>> %d'%len(buy_list))
        return buy_list
    
app = PATTERNS_LOOKER()
app.set_xfiles(['bitmex.txt'], 'joined') #'2015.txt','2016.txt','2017.txt','2018.txt'
app.load_data()
app.convert_to_digital()
digits, data = app.get_blocks(14, 'auto')
reps = app.get_patterns(digits, data, 0.3)
app.get_sell_list(reps, digits, 0.10, False)
app.get_buy_list(reps, digits, 0.10, False)

#https://leidsa.com/sorteos-anteriores
#https://www.lotoreal.com.do/resultados
#Si el valor de de orden es mayor o igual que el 50% del valor de comparacion base entonces la jugada es buena con Muchas Caidas --> (VALOR ENTERO DEL PORCENTAJE BASE)*(0.6) es el valor minimo en porciento que debe alcanzar la orden para cunplir esta linea
#Si el valor de de orden es mayor o igual que el 75% del valor de comparacion base entonces la jugada es buena con Caidas Frecuentes --> (VALOR ENTERO DEL PORCENTAJE BASE)*(0.75) es el valor minimo en porciento que debe alcanzar la orden para cunplir esta linea
#Si el valor de de orden es mayor o igual que el 85% del valor de comparacion base entonces la jugada es buena con Caidas Esporadicas
#Si el valor de de orden es mayor o igual que el 90% del valor de comparacion base entonces la jugada es buena , Pocas Caidas


