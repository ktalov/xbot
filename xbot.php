<?php
header('Access-Control-Allow-Origin: *');
header('Accept: */*');
error_reporting(E_ALL);
ini_set('display_errors', 1);
//session_start();

require_once("Bitmex.php");

class Surf extends BitMex
{
	function __construct($key='', $secret='', $timeframe='1m', $leverage = 0.01, $margin=1, $fi, $open, $close, $high, $low, $phase, $slope, $grade, $slopeLen, $orderType)
	{
		$this->bitmex = new BitMex($key, $secret);
		$this->bitmex->setLeverage($leverage);

		/*VARIABLES DE CONFIGURACION*/
		define('TIMEFRAME', $timeframe);
		define('MARGIN', $margin);
		define('LEVERAGE', $leverage);
		define('CANDLES', $this->bitmex->getCandles(TIMEFRAME, 100));
		define('SLOPE_LEN', abs($slopeLen));
		define('SLOPE_MIN', abs($slope));
		define('GRADE_MIN', abs($grade));

		/*XBTUSD  ETHUSD*/
		define('SYMBOL','XBTUSD');
		
		/*HEISIN ASHI*/
		define('LAST_FI',$fi);
		define('HAO_1',$open);
		define('HAC_1',$close);
		define('HAH_1',$high);
		define('HAL_1',$low);
		define('LAST_PHASE',$phase);
		define('LAST_ORDER_TYPE',$orderType);

		/*localhost*/
		$this->SQL_SERVER = 'localhost';
		$this->SQL_USER = 'root';
		$this->SQL_PASS = '';
		$this->DATABASE_NAME = 'surf';
	}

	public function writeHistory($what='-')
	{
		/*Escribe en el historico del proyecto
		* @param what -> Lo que quieres escribir
		*/
		$logTime = "surfRegister.html";
		$openLog = fopen($logTime, "a+");
		$inputData = $what."<br>";
		fwrite($openLog, $inputData);
		fclose($openLog);
	}

	public function saveRealSimulation($nCandles)
	{
		$conn = new mysqli($this->SQL_SERVER, $this->SQL_USER, $this->SQL_PASS, $this->DATABASE_NAME);
		mysqli_autocommit($conn, TRUE);
		
		/*Borrar datos de la tabla*/
		$sql = "TRUNCATE TABLE bitmex;";
		mysqli_query($conn, $sql);

		$candles = $this->bitmex->getCandles(TIMEFRAME, $nCandles);
		for ($i=0;$i<count($candles);$i++){
			$date = $candles[$i]['timestamp'];
			$open = $candles[$i]['open'];
			$high = $candles[$i]['high'];
			$low = $candles[$i]['low'];
			$close = $candles[$i]['close'];
			$sql = "INSERT INTO bitmex (date,open,high,low,close) VALUES ('$date','$open','$high','$low','$close');";
			mysqli_query($conn, $sql);
		}
		mysqli_close($conn);
		return $candles;
	}

	private function saveTrade($date, $open, $high, $low, $close, $type, $ROE)
	{
		$conn = new mysqli($this->SQL_SERVER, $this->SQL_USER, $this->SQL_PASS, $this->DATABASE_NAME);
		mysqli_autocommit($conn, TRUE);

		$sql = "INSERT INTO trades (date, open, high, low, close, type, margin, leverage, roe) VALUES ('$date',$open,$high,$low,$close,'$type',MARGIN,LEVERAGE,$ROE);";
		mysqli_query($conn, $sql);

		mysqli_close($conn);
	}

	private function getWalletAmount()
	{
		return $this->bitmex->getWallet()['amount']/100000000;
	}

	public function getNCandles($numCandles)
	{
		while (true){
			$cands = $this->bitmex->getCandles(TIMEFRAME, $numCandles);
			if (count($cands)<$numCandles){
				sleep(2);
				continue;
			}else{
				break;
			}
		}
		return $cands;
	}

	public function getMarketPrice()
	{
		return $this->bitmex->getTicker()['last'];
	}

	private function calculateSlope()
	{
		/*Calcula la pendiente de los ultimos 10 puntos*/
		$candles = $this->bitmex->getCandles(TIMEFRAME, SLOPE_LEN);
		$y1 = abs($candles[0]['close']-$candles[0]['open'])/2;
		$y2 = 0;
		for ($i=1;$i<count($candles);$i++){
			$y2 += abs($candles[$i]['close']-$candles[$i]['open'])/2;
		}
		$y2 = $y2/count($candles);
		$x1 = 1;
		$x2 = count($candles);
		$slope = round(($y2-$y1)/($x2-$x1),2);
		$grades = round(atan($slope)*180/M_PI,2);
		$this->writeHistory("<table border=1>
									<tr><td>SLOPE</td><td>GRADE</td></tr>
									<tr><td>$slope</td><td>$grades</td></tr>
							</table>");
		return array('slope'=>abs($slope),'grades'=>abs($grades));
	}

	private function calculateRoe()
	{
		/*Calcula el ROE de las posiciones abiertas*/

		$positions = $this->bitmex->getOpenPositions();
		$ROE = 0.0;
		if (gettype($positions) == 'array'){
			if (count($positions)>0){
				$vals = array();
				/*Calcula el ROE general de las posiciones abiertas*/
				for ($i=0;$i<count($positions);$i++){
					array_push($vals, $positions[$i]['unrealisedRoePcnt']);
				}
				if (count($vals)>0){
					$ROE = array_sum($vals)/count($vals);
					$ROE = $ROE * 100;
				}
			}
		}else{
			$ROE = 0.0;
		}
		return round($ROE,1);
	}

	public function closeAnyPosition()
	{
		/*Calcular ROE*/
		$ROE = $this->calculateRoe();

		$this->writeHistory("<strong class='good'>Closing Positions with ROE = <code>$ROE</code></strong>");

		/*Efectar venta de cualquier contrato*/
		//$this->bitmex->closePosition(null);
	}

	public function makeOrder($orderType, $fi, $high, $low, $close, $open)
	{
		/*Calcular ROE*/
		$ROE = $this->calculateRoe();

		/*Creaer nueva orden*/
		if ($orderType == 'LONG'){
			//$this->bitmex->createOrder("Market", "Buy", null, MARGIN);
		}else if($orderType == 'SHORT'){
			//$this->bitmex->createOrder("Market", "Sell", null, MARGIN);
		}else{
			$this->writeHistory("<b class='bad'>X order nothing to do| $fi</b>");
			return false;
		}

		$this->saveTrade($fi, $open, $high, $low, $close, $orderType, $ROE);
		$tab = "<div class='container-fluid table-responsive'>
					<table border=1>
						<caption>Order Created - Details</caption>
						<tbody>
							<tr>
								<td>FI</td>
								<td>OPEN</td>
								<td>HIGH</td>
								<td>LOW</td>
								<td>CLOSE</td>
								<td>TYPE</td>
								<td>ROE</td>
							</tr>
							<tr>
								<td>$fi</td>
								<td>$open</td>
								<td>$high</td>
								<td>$low</td>
								<td>$close</td>
								<td>$orderType</td>
								<td>$ROE</td>
							</tr>
						</tbody>
					</table>
				</div>";
		$this->writeHistory($tab);
	}

	public function getMatrixOrder($orderBase, $blockA, $blockB, $candleTime)
	{
		$slope_grade = $this->calculateSlope();
		$SLOPE = $slope_grade['slope'];
		$GRADE = $slope_grade['grades'];

		/*Condiciones para LONGS*/
		/*$LONGS = array('NP-VG'=>1,'NP-VR'=>1,'NP-VM'=>1,'NP-VP'=>0,
						'NR-VG'=>1,'NR-VR'=>1,'NR-VM'=>1,'NR-VP'=>1,
						'NM-VG'=>0,'NM-VR'=>1,'NM-VM'=>0,'NM-VP'=>0,
						'NG-VG'=>0,'NG-VR'=>1,'NG-VM'=>1,'NG-VP'=>0);*/

		/*Condiciones para SHORTS*/
		/*$SHORTS = array('VP-NG'=>0,'VP-NR'=>1,'VP-NM'=>1,'VP-NP'=>0,
						'VR-NG'=>1,'VR-NR'=>1,'VR-NM'=>1,'VR-NP'=>0,
						'VM-NG'=>0,'VM-NR'=>1,'VM-NM'=>0,'VM-NP'=>0,
						'VG-NG'=>1,'VG-NR'=>1,'VG-NM'=>1,'VG-NP'=>1);*/

		/*Condiciones para LONGS*/
		$LONGS = array('NP-VG'=>1,'NP-VR'=>1,'NP-VM'=>1,'NP-VP'=>1,
						'NR-VG'=>1,'NR-VR'=>1,'NR-VM'=>1,'NR-VP'=>1,
						'NM-VG'=>1,'NM-VR'=>1,'NM-VM'=>1,'NM-VP'=>1,
						'NG-VG'=>1,'NG-VR'=>1,'NG-VM'=>1,'NG-VP'=>1);

		/*Condiciones para SHORTS*/
		$SHORTS = array('VP-NG'=>1,'VP-NR'=>1,'VP-NM'=>1,'VP-NP'=>1,
						'VR-NG'=>1,'VR-NR'=>1,'VR-NM'=>1,'VR-NP'=>1,
						'VM-NG'=>1,'VM-NR'=>1,'VM-NM'=>1,'VM-NP'=>1,
						'VG-NG'=>1,'VG-NR'=>1,'VG-NM'=>1,'VG-NP'=>1);

		$this->writeHistory("BLOCK | $blockA-$blockB");
		if ($orderBase == 'LONG'){
			if ($SLOPE >= SLOPE_MIN or $GRADE >= GRADE_MIN){
				return $LONGS["$blockA-$blockB"];
			}else{
				$this->writeHistory("<b class='bad parpadea'>Low slope($SLOPE) and grade($GRADE) at $candleTime </b>");
				return 0;
			}
		}else{
			if ($SLOPE >= SLOPE_MIN or $GRADE >= GRADE_MIN){
				return $SHORTS["$blockA-$blockB"];
			}else{
				$this->writeHistory("<b class='bad parpadea'>Low slope($SLOPE) and grade($GRADE) at $candleTime </b>");
				return 0;
			}
		}
	}

	public function getCandleForm($progressOrder, $open, $close)
	{
		/*Devuelve el color y tamano del candle*/
		$size = abs($close - $open);
		switch(TIMEFRAME){
			case '1m':
				/*Establece el tamano -- de acuerdo al TIMEFRAME */
				if ($size <= 1){
					$size = 'P';
				}else if($size <=3){
					$size = 'R';
				}else if($size <= 5){
					$size = 'M';
				}else{
					$size = 'G';
				}
				break;
			case '5m':
				/*Establece el tamano -- de acuerdo al TIMEFRAME */
				if ($size <= 3){
					$size = 'P';
				}else if($size <=15){
					$size = 'R';
				}else if($size <= 30){
					$size = 'M';
				}else{
					$size = 'G';
				}
				break;
			default:
				$size = 'P';
		}

		/*Establece el color*/
		switch($progressOrder){
			case 'LONG':
				$color = 'V';
				break;
			case 'SHORT':
				$color = 'N';
				break;
			default:
				$color = '-';
		}
		return "$color"."$size";
	}

	public function startSurfing()
	{
		$slope_grade = $this->calculateSlope();
		$SLOPE = $slope_grade['slope'];
		$GRADE = $slope_grade['grades'];
		$ROE = strval($this->calculateRoe());
		$this->writeHistory("<strong class='parpadea good'>Starting surf...</strong>");
		if (LAST_PHASE == 1){
			$this->writeHistory("<b class='good'>Inicio de verificaciones</b>");

			$logTime = "surfRegister.html";
			$openLog = fopen($logTime, "w");
			$inputData = "<b>REINICIO</b><br>";
			fwrite($openLog, $inputData);
			fclose($openLog);

			/*Esta fase toma 2 valores y determina parcialmente cual es la pendiente que sigue la curva*/
			/*Solicita 2 candles*/
			$candles = $this->getNCandles(2);

			$fi = $candles[0]['timestamp'];
			$open = $candles[0]['open'];
			$close = $candles[0]['close'];
			$high = $candles[0]['high'];
			$low = $candles[0]['low'];

			$HAC_0 = round(($candles[0]['close']+$candles[0]['open']+$candles[0]['high']+$candles[0]['low'])/4,1);
			$HAO_0 = round(($open + $close)/2,1);
			$HAH_0 = round($high,1);
			$HAL_0 = round($low,1);

			$HAC_1 = round(($candles[1]['close']+$candles[1]['open']+$candles[1]['high']+$candles[1]['low'])/4,1);
			$HAO_1 = round(($candles[1]['open'] + $candles[1]['close'])/2,1);
			$HAH_1 = round($candles[1]['high'],1);
			$HAL_1 = round($candles[1]['low'],1);

			/*Preparando los nuevos valores para cuando vuelva a ser llamada la clase.Se toma la fecha anterior a la nueva o que se esta calculando.*/
			/*Determina que hacer de acuerdo al valor de los candles obtenidos*/
			if ($HAC_0 >= $HAC_1){
				$orderType = 'LONG';
			}else{
				$orderType = 'SHORT';
			}
			$this->writeHistory(" <code>INITIALIZATION : $orderType </code>");

			/*Crear una orden*/
			$this->makeOrder($orderType, $fi, $high, $low, $close, $open);

			return array('ret_message'=>"F1 completada",'ret_status'=>true,'ret_candles'=>$candles,'fi'=>$fi,'phase'=>2,'high'=>$HAH_0,'low'=>$HAL_0,'close'=>$HAC_0,'open'=>$HAO_0,'orderType'=>$orderType,'WALLET_AMOUNT'=>$this->getWalletAmount(),'candles'=>CANDLES, 'SLOPE'=>$SLOPE, 'GRADE'=>$GRADE,'roe'=>$ROE,'TRIGGUER_CLOSE'=>$HAC_0,'BEFORE_CLOSE'=>$HAC_1);
		}else{

			/*Solicitar los ultimos 2 candles*/
			$candles = $this->getNCandles(2);

			/*Verifica la fecha de cuando se efectuo la operacion anterior existe.Si existe es porque aun no se ha cerrado el candle que le seguia.El candle que seguia al ser cerrado es lo que tomamos como indicador para saber si hacer SELL o continuar la pendiente a favor*/
			$EXIST = false;
			for ($h = 0; $h<count($candles); $h++){
				if (LAST_FI === $candles[$h]['timestamp']){
					$EXIST = true;
					break;
				}else{
					continue;
				}
			}

			if ($EXIST){
				$ret_message = "<b class='bad'>Waiting for close of TRIGGER_CANDLE ...</b>";
				$this->writeHistory($ret_message);

				/*Mantener el dato justo como llego*/
				return array('ret_message'=>$ret_message,'ret_status'=>true,'ret_candles'=>$candles,'fi'=>LAST_FI,'phase'=>LAST_PHASE,'high'=>HAH_1,'low'=>HAL_1,'close'=>HAC_1,'open'=>HAO_1,'orderType'=>LAST_ORDER_TYPE,'WALLET_AMOUNT'=>$this->getWalletAmount(),'candles'=>CANDLES, 'SLOPE'=>$SLOPE, 'GRADE'=>$GRADE,'roe'=>$ROE);
			}else{
				/*SI YA NO EXISTE, quiere decir que el candle que le sigue ya CERRO.Ese candle deberia ser ,en el mejor caso,el de fecha mas vieja es decir el $candle[1]*/
				$TRIGGUER_CANDLE = $candles[1];
				$HAC_0 = round(($TRIGGUER_CANDLE['close']+$TRIGGUER_CANDLE['open']+$TRIGGUER_CANDLE['high']+$TRIGGUER_CANDLE['low'])/4,1);
				$HAO_0 = round((HAO_1+HAC_1)/2,1);
				$HAH_0 = round(max($TRIGGUER_CANDLE['high'],$HAO_0,$HAC_0),1);
				$HAL_0 = round(min($TRIGGUER_CANDLE['low'],$HAO_0,$HAC_0),1);
				$TRIGGER_FI = $TRIGGUER_CANDLE['timestamp'];

				/*Verifica cual era el tipo de orden efectuada anteriormente y determina que hacer en base a la tendencia de trade actual*/
				$this->writeHistory("<table class='table' border=1>
										<caption>Condicion Actual</caption>
										<tr>
											<td>Cierre Anterior</td>
											<td>Cierre del Trigger</td>
											<td>Tipo de Orden en Ejecuci&oacute;n</td>
										</tr>
										<tr>
											<td>".HAC_1."</td>
											<td>$HAC_0</td>
											<td>".LAST_ORDER_TYPE."</td>
										</tr>
									</table>");
				switch(LAST_ORDER_TYPE){
					case 'LONG':
						if ( HAC_1 > $HAC_0 ){
							$lastFormCandle = $this->getCandleForm(LAST_ORDER_TYPE, HAO_1, HAC_1);
							$trigFormCandle = $this->getCandleForm('SHORT',$HAO_0,$HAC_0);
							$change = $this->getMatrixOrder('SHORT',$lastFormCandle,$trigFormCandle,$TRIGGER_FI);
							$change = 1;
							if($change){
								/*Cambio de trade, paso a SHORT*/
								$newOrderType = 'SHORT';
								$orderChange = true;
								$ret_message = "<table class='table' border=1>
													<caption><code class='parpadea'>Cambio de Orden at $TRIGGER_FI</code></caption>
													<tr>
														<td>Orden Anterior</td>
														<td>Nueva Orden</td>
														<td>Candle</td>
														<td>Roe Actual</td>
													</tr>
													<tr>
														<td>".LAST_ORDER_TYPE."</td>
														<td>$newOrderType</td>
														<td>$TRIGGER_FI</td>
														<td>$ROE</td>
													</tr>";
							}else{
								/*Las caracteristicas de los candles no aplican para cambio*/
								$orderChange = false;
								$newOrderType = LAST_ORDER_TYPE;
								$ret_message = "<b class='good'>LONG(1) continue in progress at $TRIGGER_FI with ROE=$ROE</b>";
							}
						}else{
							$orderChange = false;
							$newOrderType = LAST_ORDER_TYPE;
							$ret_message = "<b class='good'>LONG(2) continue in progress at $TRIGGER_FI with ROE=$ROE</b>";
						}
						break;
					case 'SHORT':
						if ( HAC_1 < $TRIGGUER_CANDLE['close'] ){
							$lastFormCandle = $this->getCandleForm(LAST_ORDER_TYPE, HAO_1, HAC_1);
							$trigFormCandle = $this->getCandleForm('LONG',$HAO_0,$HAC_0);
							$change = $this->getMatrixOrder('LONG',$lastFormCandle,$trigFormCandle,$TRIGGER_FI);
							$change = 1;
							if($change){
								/*Cambio de trade, paso a LONG*/
								$newOrderType = 'LONG';
								$orderChange = true;
								$ret_message = "<table class='table' border=1>
													<caption ><code class='parpadea'>Cambio de Orden at $TRIGGER_FI</code></caption>
													<tr>
														<td>Orden Anterior</td>
														<td>Nueva Orden</td>
														<td>Candle</td>
														<td>Roe Actual</td>
													</tr>
													<tr>
														<td>".LAST_ORDER_TYPE."</td>
														<td>$newOrderType</td>
														<td>$TRIGGER_FI</td>
														<td>$ROE</td>
													</tr>";
							}else{
								$orderChange = false;
								$newOrderType = LAST_ORDER_TYPE;
								$ret_message = "<b class='good'>SHORT(1) continue in progress at $TRIGGER_FI with ROE=$ROE</b>";
							}
						}else{
							$orderChange = false;
							$newOrderType = LAST_ORDER_TYPE;
							$ret_message = "<b class='good'>SHORT(2) continue in progress at $TRIGGER_FI with ROE=$ROE</b>";
						}
						break;
					default:
						$ret_message = "<b class='bad'>-No hay una orden anterior definida- Se determinara una adecuada para el candle -$TRIGGER_FI-</b>";

						/*Sino hay un tipo de orden especificada entonces se determina uno*/
						$this->writeHistory($ret_message);

						/*Esta fase toma 2 valores y determina parcialmente cual es la pendiente que sigue la curva*/
						/*Solicita 2 candles*/
						$candles = $this->getNCandles(2);
						
						$fi = $candles[0]['timestamp'];
						$open = $candles[0]['open'];
						$close = $candles[0]['close'];
						$high = $candles[0]['high'];
						$low = $candles[0]['low'];

						$HAC_0 = round(($candles[0]['close']+$candles[0]['open']+$candles[0]['high']+$candles[0]['low'])/4,1);
						$HAO_0 = round(($open + $close)/2,1);
						$HAH_0 = round($high,1);
						$HAL_0 = round($low,1);

						$HAC_1 = round(($candles[1]['close']+$candles[1]['open']+$candles[1]['high']+$candles[1]['low'])/4,1);
						$HAO_1 = round(($candles[1]['open'] + $candles[1]['close'])/2,1);
						$HAH_1 = round($candles[1]['high'],1);
						$HAL_1 = round($candles[1]['low'],1);

						/*Preparando los nuevos valores para cuando vuelva a ser llamada la clase.Se toma la fecha anterior a la nueva o que se esta calculando.*/
						/*Determina que hacer de acuerdo al valor de los candles obtenidos*/
						if ($HAC_0 >= $HAC_1){
							$orderType = 'LONG';
						}else{
							$orderType = 'SHORT';
						}

						/*Crear una orden*/
						$this->closeAnyPosition();
						$this->makeOrder($orderType, $fi, $high, $low, $close, $open);

						return array('ret_message'=>$ret_message,'ret_status'=>true,'ret_candles'=>$candles,'fi'=>$fi,'phase'=>2,'high'=>$HAH_0,'low'=>$HAL_0,'close'=>$HAC_0,'open'=>$HAO_0,'orderType'=>$orderType,'WALLET_AMOUNT'=>$this->getWalletAmount(),'candles'=>CANDLES, 'SLOPE'=>$SLOPE, 'GRADE'=>$GRADE, 'roe'=>$ROE,'TRIGGUER_CLOSE'=>$HAC_0,'BEFORE_CLOSE'=>$HAC_1);
				}
				$this->writeHistory($ret_message);

				/*Verifica si hay que cambiar la orden*/
				if ($orderChange == true){
					$this->closeAnyPosition();
					$this->makeOrder($newOrderType, $TRIGGUER_CANDLE['timestamp'], $TRIGGUER_CANDLE['high'], $TRIGGUER_CANDLE['low'], $TRIGGUER_CANDLE['close'], $TRIGGUER_CANDLE['open']);
				}

				return array('ret_message'=>$ret_message,'ret_status'=>true,'ret_candles'=>$candles,'fi'=>$TRIGGUER_CANDLE['timestamp'],'phase'=>LAST_PHASE,'high'=>$HAH_0,'low'=>$HAL_0,'close'=>$HAC_0,'open'=>$HAO_0,'orderType'=>$newOrderType,'WALLET_AMOUNT'=>$this->getWalletAmount(), 'candles'=>CANDLES, 'SLOPE'=>$SLOPE, 'GRADE'=>$GRADE, 'roe'=>$ROE,'TRIGGUER_CLOSE'=>$HAC_0,'BEFORE_CLOSE'=>HAC_1);
			}
		}
	}
}

if ($_SERVER["REQUEST_METHOD"] == 'GET'){
	if (isset($_GET['strategy'])){
		switch($_GET['strategy']){
			case 'SURF_BOT':
				$bot = new Surf($_GET['key'], $_GET['secret'], $_GET['timeframe'], $_GET['leverage'], $_GET['margin'],$_GET['fi'], $_GET['open'],$_GET['close'],$_GET['high'],$_GET['low'], $_GET['phase'],$_GET['slope'],$_GET['grade'],$_GET['slopeLen'],$_GET['orderType'] );
				$res = $bot->startSurfing();
				break;
			case 'CAPTURE':/*localhost:8080/bot/xbot.php?strategy=CAPTURE&key=FKwqoldOB2fIF-0eBI_8XJd5&secret=EdJ8vVejU2I4qakE3IE7rves55rTBVVnOxlhVisZMXfUyIKr&timeframe=1m&candles=1000&margin=1&leverage=100&fi=&phase=&open=&close=&high=&low=&slope=0.1&grade=10&slopeLen=4&orderType=*/
				$bot = new Surf($_GET['key'], $_GET['secret'], $_GET['timeframe'], $_GET['leverage'], $_GET['margin'],$_GET['fi'], $_GET['open'],$_GET['close'],$_GET['high'],$_GET['low'], $_GET['phase'],$_GET['slope'],$_GET['grade'],$_GET['slopeLen'],$_GET['orderType'] );
				$res = $bot->saveRealSimulation($_GET['candles']);
				break;
			default:
				$res = array('ANSWER'=>$_GET['strategy']);
		}
	} else {
		$res = array('ANSWER'=>'N/A');
	}
	//Retornar el resultado
	echo json_encode($res);
}
?>