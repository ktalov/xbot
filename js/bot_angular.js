var bot_app = angular.module('bot_app', ['ngRoute','ngCookies','ngAnimate', 'ngSanitize', 'ui.bootstrap','zingchart-angularjs']);
var server = 'http://localhost:8080/';

var url_base = '../bot/';
var url_redirect = '../bot/';

//HTTPPROVIDER CONFIGURATION
bot_app.config(['$httpProvider',
	function($httpProvider){
			$httpProvider.defaults.withCredentials = true;
		}
	]
);

//RUTAS
bot_app.config(
	function($routeProvider){
		$routeProvider
			.when(
					'/',
					{
						templateUrl:url_base + 'control.html',
						controller:'control'
					}
				);
	}
);

//SWEET ALERT SERVICE
bot_app.factory(
	'SweetAlert', 
	[ 
	'$rootScope',
	function ( $rootScope ){
			var swal = window.swal;
			//public methods
			var self = 
			{
				swal: function ( arg1, arg2, arg3)
						{
							$rootScope.$evalAsync(
								function()
								{
									if( typeof(arg2) === 'function' ) {
										swal(arg1, 
											function(isConfirm)
											{
												$rootScope.$evalAsync( 
													function()
														{
															arg2(isConfirm);
														}
												);
											}, 
												arg3 
											);
									} else {
										swal(arg1, arg2, arg3);
									}
								}
								);
						},
			success: function(title, message)
				{
					$rootScope.$evalAsync(function(){
							swal(title, message, 'success');
						}
					);
				},
			error: function(title, message)
				{
					$rootScope.$evalAsync(
						function(){
								swal(title, message, 'error');
							});
				},
			warning: function(title, message)
				{
					$rootScope.$evalAsync(function(){
						swal(title, message, 'warning');
					});
				},
			info: function(title, message)
				{
					$rootScope.$evalAsync(function(){
						swal(title, message, 'info');
						}
					);
				}
			};
			return self;
		}
	]
);

//GLOBAL SCOPES
bot_app.run(
	function($rootScope, $http, $window, $cookies, $location)
	{
			$rootScope.USER_MAIL = '';
			$rootScope.USER_PASS = '';

			/*Cierre de sesion*/
			$rootScope.delogPUBLIC_ID = function()
			{
			};

			/*Obtener correo del usuario*/
			$rootScope.askforDATA = function()
			{
			};

			/*Obtener todo*/
			$rootScope.askforALL = function()
			{
			};
		}
	);

bot_app.controller('control', 
	function($rootScope, $scope, $log, $window, $http, $cookies, $location, $timeout, $interval, $filter)
	{
		$scope.myJson = {
  type: "area",
  series: [
    { values: [20,40,25,50,15,45,33,34]}
  ]
};
		$scope.message = 'stop';
		$scope.xbot = 'SURF_BOT';
		$scope.theinterval = false;
		$scope.leverage = 0.01;
		$scope.frames = Array({'frame':'1m','val':1}, {'frame':'5m','val':5}, {'frame':'1h','val':60});
		$scope.amount = 1;
		$scope.timerefresh = 2.0;
		$scope.timeframe = '5m';
		$scope.fi = '';
		$scope.phase = 1;
		$scope.open = 0;
		$scope.close = 0;
		$scope.high = 0;
		$scope.low = 0;
		$scope.slope = 0.0;
		$scope.grade = 0.0;
		$scope.slopeLen = 4;
		$scope.orderType = '';
		/*Tesnet*/
		$scope.key = 'KXlTQLjqXjFaLlQiPngrtSVQ';
		$scope.secret = 'rwEM_BfchcrG7bjlZNaK76eOInU9ap9iSoQn9-MAg7mtSstj';
		/*bitmex*/
		//$scope.key = 'FKwqoldOB2fIF-0eBI_8XJd5';
		//$scope.secret = 'EdJ8vVejU2I4qakE3IE7rves55rTBVVnOxlhVisZMXfUyIKr';
		/**/
		$scope.repeat = 0;
		$scope.wallet_amount = 0.0;
		$scope.btn_toogle = true;

		$scope.open_testnet = function()
		{
			$window.open('https://testnet.bitmex.com','_blank','resizable=yes,width=800,height=600');
		};

		$scope.open_bitmex = function()
		{
			$window.open('https://bitmex.com','_blank','resizable=yes,width=800,height=600');
		};

		$scope.open_calculator = function()
		{
			$window.open(`https://www.xe.com/currencyconverter/convert/?Amount=${$scope.wallet_amount}&From=XBT&To=USD`,'_blank','resizable=no,width=400,height=600,left=200,top=50');
		};

		$scope.log = function()
		{
			//$window.open(server + '../bot/weed_bot_register.html','_blank','resizable=no,width=600,height=600,left=200,top=50');

			$http({
				method: 'GET',
				url: server + '../bot/surfRegister.html',
				withCredentials:false,
				headers:{'Access-Control-Allow-Origin':true,
					'Access-Control-Allow-Credentials':false
				}
			}).then(
				function(response)
				{
					document.getElementById('console').innerHTML = response.data + document.getElementById('console').innerHTML;
				},
				function myError(response)
				{
					document.getElementById('console').innerHTML = response.data + document.getElementById('console').innerHTML;
				}
			);
		};

		$scope.exec_manual = function()
		{
			console.log('EJECUCION MANUAL');
		};

		$scope.console_write = function(what, console)
		{
			document.getElementById(console).innerHTML = what + document.getElementById(String(console)).innerHTML;
		};
		
		$scope.clear_console = function(console)
		{
			document.getElementById(console).innerHTML = '';
		};

		$scope.manual_do = function(order)
		{
			if (confirm('Seguro que desea efectuar esta accion?, no hay vuelta atras')){
				$http({
					method: 'GET',
					url: server + '../bot/xbot.php',
					withCredentials:false,
					headers:{'Access-Control-Allow-Origin':true,
						'Access-Control-Allow-Credentials':false
					},
					params:{
						'strategy':'MANUAL',
						'key':$scope.key,
						'secret':$scope.secret,
						'roe':$scope.roe,
						'order':order
					}
				}).then(
					function(response)
					{
						$scope.ret = response.data;
						let keys = Object.keys($scope.ret);
						
						//Imprime en el elemento mencionado los valores devueltos en la respuesta
						document.getElementById('strategy_data').innerHTML = '';
						let content = "";
						for (k of keys){
							$scope.console_write(`<code>${k.toUpperCase()}</code>:${$scope.ret[k]}<br>`,'console');
						}
					},
					function myError(response){
						console.log(response.data);
					}
				);
			}else{
				sweetAlert({
					title: 'MANUAL',
					text: 'Usted a cancelado la transaccion',
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Cerrar',
					closeOnConfirm: true
				});
			}
		};

		$scope.stop = function(show)
		{
			$scope.btn_toogle = true;
			if ($scope.theinterval){
				$interval.cancel($scope.theinterval);
				$scope.theinterval = false;
				$scope.message = 'stop';
				if (show){
					sweetAlert({
						title: 'BOT',
						text: 'El BOT ha sido detenido',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#DD6B55',
						confirmButtonText: 'Cerrar',
						closeOnConfirm: true
					});
				}else{
					$scope.console_write('<b>El BOT ha sido detenido</b><br>','console');
				}
			}else{
				if (show){
					sweetAlert({
						title: 'BOT',
						text: 'No hay procesos ejecutandose',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#DD6B55',
						confirmButtonText: 'Cerrar',
						closeOnConfirm: true
					});
				}else{
					if ($scope.repeat != 0){
						$scope.console_write('<b>No hay procesos ejecutandose</b><br>','console');
					}
				}
			}
			
		};

		$scope.start = function()
		{
			$scope.btn_toogle = false;
			$scope.message = 'inicio';

			sweetAlert({
				title: 'BOT',
				text: 'Inicializacion del BOT,debe esperar que se cumpla el tiempo de loop para hacer la primera fase.Buena suerte y que los dioses le protejan!',
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Cerrar',
				closeOnConfirm: true
			});

			if (typeof $scope.timerefresh === 'undefined'){
				$scope.timerefresh = 5;
			}
			if (typeof $scope.xbot === 'undefined'){
				sweetAlert({
					title: 'BOT',
					text: 'Indique una estrategia *BOT*',
					type: 'error',
					showCancelButton: false,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Cerrar',
					closeOnConfirm: true
				});
				return false;
			};

			if (typeof $scope.key === 'undefined' || typeof $scope.secret === 'undefined'){
				sweetAlert({
					title: 'CREDENCIALES',
					text: 'Complete las credenciales KEY &| SECRET_KEY',
					type: 'error',
					showCancelButton: false,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Cerrar',
					closeOnConfirm: true
				});
				return false;
			};

			if (!$scope.theinterval){
				document.getElementById('console').innerHTML = '';
				/*Llena la consola con estos datos*/
				/*let parm = {	'date':new Date(),
								'strategy':$scope.xbot,
								'key':$scope.key,
								'secret':$scope.secret,
								'leverage':$scope.leverage,
								'timeframe':$scope.timeframe,
								'margin':$scope.amount,
								'fi':$scope.fi,
								'phase':$scope.phase,
								'open':$scope.open,
								'close':$scope.close,
								'high':$scope.high,
								'low':$scope.low,
								'orderType':$scope.orderType};
				let ks = Object.keys(parm);
				for (k of ks){
					$scope.console_write(`<code>${k.toUpperCase()}</code>:${parm[k]}<br>`,'console');
				}
				$scope.log();*/

				$scope.theinterval = $interval(function(){
					$scope.message = 'pendiente';
					$http({
						method: 'GET',
						url: server + '../bot/xbot.php',
						withCredentials:false,
						headers:{'Access-Control-Allow-Origin':true,
							'Access-Control-Allow-Credentials':false
						},
						params:{
							'strategy':$scope.xbot,
							'key':$scope.key,
							'secret':$scope.secret,
							'leverage':$scope.leverage,
							'timeframe':$scope.timeframe,
							'margin':$scope.amount,
							'fi':$scope.fi,
							'phase':$scope.phase,
							'open':$scope.open,
							'close':$scope.close,
							'high':$scope.high,
							'low':$scope.low,
							'slope':$scope.slope,
							'grade':$scope.grade,
							'slopeLen':$scope.slopeLen,
							'orderType':$scope.orderType
						}
					}).then(
						function(response){
							$scope.ret = response.data;
							$scope.fi = $scope.ret.fi;
							$scope.phase = $scope.ret.phase;
							$scope.open = $scope.ret.open;
							$scope.close = $scope.ret.close;
							$scope.high = $scope.ret.high;
							$scope.low = $scope.ret.low;
							$scope.orderType = $scope.ret.orderType;
							$scope.cands = $scope.ret.candles;
							console.log('CANDLES');
							console.log($scope.ret.ret_candles);

							/*$scope.myJson.series[0].values = [];
							$scope.dots = Array();
							var open = 0;
							var high = 0;
							var low = 0;
							var close = 0;
							for (let i=-1;i<$scope.cands.length;i++){
								try{
									var closeA = $scope.cands[i].close;
									var closeB = $scope.cands[i+1].close;
									if (closeA <= closeB){
										$scope.dots.push(-10);
									}else{
										$scope.dots.push(10);
									}
								}catch{
									console.log('End');
								}
							}

							$scope.myJson.series[0].values = $scope.dots;*/
							$scope.message = 'completado';

							//Presenta los valores de la respuesta
							document.getElementById('strategy_data').innerHTML = '';
							let content = "";
							let keys = Object.keys($scope.ret);
							for (k of keys){
								if (k == 'ret_candles' || k == 'ret_message' || k == 'ret_status' || k == 'candles'){
									continue;
								}
								content += `<tr><td class='resp'>${k.toUpperCase()}</td><td>${$scope.ret[k]}</td></tr>`;
								/*Coloca el monto actual de dinero ,en XBT, que posees en tu cuenta*/
								if (k == 'WALLET_AMOUNT'){
									$scope.wallet_amount = $scope.ret[k];
								}
							}
							document.getElementById('strategy_data').innerHTML = content;
							document.getElementById('console').innerHTML = '';
							$scope.log();
						},
						function myError(response){
							console.log(response.data);
						}
					);
					$scope.repeat += 1;
				},1000*$scope.timerefresh*60);
			}else{
				console.log('Error creating');
			}
		};
	}
);