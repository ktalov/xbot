<!DOCTYPE html>
<?php
	header('Access-Control-Allow-Origin: *');
	header('Accept: */*');
?>
<?php
	session_start();
?>
<html lang = 'es-DO'>
	<head>
		<title>surfBot</title>
		<meta http-equiv = 'Content-Type' content = 'text/html; charset=utf-8' />
		<meta name = 'viewport' content = 'width=device-width, initial-scale=1' />
		<meta name = 'description' content = 'Bot Bitmex'/>
		<meta name = 'keywords' content = 'Bot Bitmex'/>
		<meta name = 'author' content = 'surfBot'/>
		<meta name = 'robots' content = 'Index, Follow'/>
		<meta name = 'title' content = 'Bot Bitmex'/>
		<meta name = 'url' content = ''/>
		<meta name = 'identifier-URL' content = ''/>
		<meta name = 'revised' content = 'Mondat, May 06th, 2019, 14:53'/>
		<meta name = '' content = 'Bot Bitmex'/>
		<meta name = 'language' content = 'es'/>
		<meta itemprop = 'name' content = 'Bot'/>
		<meta name = 'description' content = 'surfBot' />
		<meta itemprop = 'description' content = 'surfBot' />

		<base href = '../bot/' target = '_blank'>
		<!--base href = '' target = '_blank'-->

		<link rel = 'shortcut icon' type = 'image/png' href = 'images/icons/surf.png'>
		
		<!--JQUERY MIN JS-->
		<script src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
		<!--script src = 'js/jquery/jquery.min.js'></script-->
		
		<!--BOOTSTRAP MIN JS-->
		<script src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
		<!--script src = 'js/bootstrap/bootstrap.min.js'></script-->
		
		<!--BOOTSTRAP MIN CSS-->
		<link rel = 'stylesheet' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
		<!--link rel = 'stylesheet' href = 'css/bootstrap/bootstrap.min.css'-->

		<!--FONTSAWASOME-->
		<link rel = 'stylesheet' href = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
		<!--link rel = 'stylesheet' href = 'css/fonts/font-awesome/4.7.0/css/font-awesome.min.css'-->

		<!-- SHA1 , MD5-->
		<script type = 'text/javascript' src = 'js/cypher/md5.js'></script>
		<script type = 'text/javascript' src = 'js/cypher/sha1.js'></script>

		<!-- ANGULARJS-->
		<!--script src = 'js/angularjs/1.6.9/angular.min.js'></script-->
		<!--script src = 'js/angularjs/1.6.9/angular-route.js'></script-->
		<!--script src = 'js/angularjs/1.6.9/angular-cookies.js'></script-->
		<script src = 'https://ajax.googleapis.com/ajax/libs/angularjs/1.7.3/angular.min.js'></script>
		<script src = 'https://ajax.googleapis.com/ajax/libs/angularjs/1.7.3/angular-route.js'></script>
		<script src = 'https://ajax.googleapis.com/ajax/libs/angularjs/1.7.3/angular-animate.js'></script>
		<script src = 'https://ajax.googleapis.com/ajax/libs/angularjs/1.7.3/angular-sanitize.js'></script>
		<script src = 'https://ajax.googleapis.com/ajax/libs/angularjs/1.7.3/angular-cookies.js'></script>
		<script src = 'http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js'></script>

		<!-- CHARTS -->
		<script src="node_modules/zingchart/zingchart.min.js"></script>
		<script src="node_modules/zingchart-angularjs/src/zingchart-angularjs.js"></script>

		<!-- SWEET ALERT CSS-->
		<link rel = 'stylesheet' type = 'text/css' href = 'css/sweetalert/sweet-alert.css'>

		<!--SWEET ALERT JS-->
		<script type = 'text/javascript' src = 'js/sweetalert/sweet-alert.min.js'></script>

		<!-- HIGHCHARTS -->
		<script src = 'js/highcharts/highcharts.js'></script>
		<script src = 'js/highcharts/highcharts-more.js'></script>

		<!-- GENERALS CSS-->
		<link rel = 'stylesheet' type = 'text/css' href = 'css/bot.css'>
	</head>
	<body class = 'container-fluid' id = 'generalbody' ng-app = 'bot_app'>
		<!-- NAVIGATOR TOP-->
		<nav id = 'top_navigator' class = 'navbar navbar-inverse navbar-fixed-top' style = 'margin:0px;height:50px;padding:5px;' ng-hide = 'loginit'>
			<div class = 'container-fluid' style = 'padding:0px'>
				<div class = 'navbar-header container-fluid'>
					<img class = 'img-responsive visible-md visible-lg hidden-sm hidden-xs' src = 'images/icons/surf.png' alt = 'surf.png' style = 'width:50px;float:left;'>
					<div class='container-fluid' style = 'margin-top:10px;float:left'>
						<b class='shkoderblack parpadea' style = 'font-size:18px;color:green;cursor:pointer'>surfBot</b>
					</div>
				</div>
			</div>
		</nav>
		<!-- GENERALES-->
		<div id = 'contentbody' class = 'main container-fluid' style="margin:0px;padding:0px;background: url('images/backgrounds/scrubble.png');
	background-position:center;
	background-repeat:no-repeat;
	background-size:contain;
	background-attachment: fixed;
	min-height:625px">
			<div ng-view>
			</div>
		</div>
		<script type = 'text/javascript'  src = 'js/bot_angular.js'></script>
	</body>
</html>